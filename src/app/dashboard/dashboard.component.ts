import { Component, OnInit, ViewChild } from '@angular/core';

import { ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexFill, ApexLegend, ApexPlotOptions, ApexResponsive, ApexXAxis, ChartComponent } from 'ng-apexcharts';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  xaxis: ApexXAxis;
  legend: ApexLegend;
  fill: ApexFill;
};
@Component({
  selector: 'app-dashboard',
  templateUrl:'./dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  // <@ViewChild("chart") chart: ChartComponent;>
  public chartOptions: Partial<ChartOptions> |any;
  public studentData:any={
    studentname:"murali"
  }
  public labels=[{
    id:1,
    name:"AA"
},{
  id:2,
  name:"BB"
},{
  id:3,
  name:"CC"
}]
  public assignarray=[
    {
      id:1,
      name:'A'
    },
    {
      id:2,
      name:'B'
    },
    {
      id:3,
      name:'C'
    },
    {
      id:4,
      name:'D'
    },
    {
      id:5,
      name:'E'
    },
    {
      id:6,
      name:'F'
    }
  ];
  constructor() {
    this.chartOptions = {
      series: [
        {
          name:[],
        data:[]
      }
       
      ],
      labels: [],
      chart: {
        type: "bar",
        height: 350,
        stacked: true,
        toolbar: {
          show: true
        },
        zoom: {
          enabled: true
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      xaxis: {
        type: "category",
        categories: [
         
        ]
      },
      legend: {
        position: "right",
        offsetY: 40
      },
      fill: {
        opacity: 1
      }
    };
  }


   

  ngOnInit(): void {
    for(var val of this.assignarray){debugger
    this.chartOptions.series[0].data.push(val.id); 
    this.chartOptions.xaxis.categories.push(val.name);  
    

  }
  for(var val of this.labels){
    this.chartOptions.series[0].name=val.name; 
  }
  
  
}
  

}
