import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public login:any;
public username="admin";
public password="admin";

constructor(private fb:FormBuilder ,private router :Router ) { }
  public submitted = false;
  ngOnInit(): void {
    this.login=this.fb.group({
      username: ['', Validators.required],      
      password: ['', Validators.required],
    })
  }
  Login(){debugger
    console.log(this.login)
    if(this.login.status == "INVALID"){
      this.submitted = true;
      return;
    }else{
      if(this.login.value.username == this.username && this.login.value.password == this.password){
        console.log("login success")
        alert("login success")
        this.router.navigate(['dashboard']);
      }else{
        console.log("wrong credentails")
        alert("wrong credentials")
        this.router.navigate(['login']);

      }
    }
  }

  get form() {
    return this.login.controls;
  }

}


